#!/usr/bin/env python3
# Created by Galen Nare 5/18/2021
# ELEG 305 Computer Assignment 5

import numpy as np
import os
import scipy.io

from matplotlib import pyplot

fname_fmt = r'100m ({0}).mat'

signal_time_s = 10
sample_rate = 360

scale_factor = 100
offset = 1000


# Function alias to load Matlab .mat signal files
def load_signal(filename: str):
    return scipy.io.loadmat(filename)


# Function to process and plot signal data from loaded .mat files
def process(sig_input: dict, idx: int):
    y1_t = sig_input['val'][0]
    t = np.linspace(0, signal_time_s, int(signal_time_s * sample_rate))  # t = 1-10sec: 3600 samples @ 360 Hz
    pyplot.plot(t, y1_t, label=fname_fmt.format(idx))
    pyplot.ylabel('amplitude (µV)')
    pyplot.xlabel('time (s)')

    beats = 0
    skip = 0
    first_peak_t = 0
    for i in range(len(y1_t)):  # Count beats for averaging BPM
        sample = y1_t[i]
        if skip:
            skip -= 1
            continue
        if sample >= 1100:
            skip += 10  # Jump ahead 10 samples to avoid counting the same peak multiple times
            if beats == 0:
                first_peak_t = i / sample_rate  # Used to align sine wave to first peak
            beats += 1

    avg_bpm = beats * (60 / signal_time_s)
    avg_ht = np.average(y1_t) + 100  # Used to help offset the sine wave on Y axis.

    y2_t = scale_factor * np.cos(2 * np.pi * avg_bpm / 60 * (t - first_peak_t)) + avg_ht  # Sine wave equation
    pyplot.plot(t, y2_t, label=r'$y2(t) = {0} cos(2 \pi  * {1} / {2} * (t - {3})) + {4}$'
                .format(scale_factor, round(avg_bpm, 4), 60, round(first_peak_t, 4),
                        round(avg_ht, 4)))

    # Plot setup and polishing
    print('average bpm:', avg_bpm)
    pyplot.title('Plot of ' + fname_fmt.format(idx) + ' (Average BPM: ' + str(avg_bpm) + ')')
    pyplot.legend()
    pyplot.show()


for i in range(0, 10):
    signal = load_signal(os.path.join('MLII', '1 NSR', fname_fmt.format(i)))
    print('processing signal', i, ':', signal)
    process(signal, i)
